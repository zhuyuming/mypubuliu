﻿# Host: 127.0.0.1  (Version: 5.5.21)
# Date: 2012-12-07 06:40:12
# Generator: MySQL-Front 5.3  (Build 1.18)

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE */;
/*!40101 SET SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES */;
/*!40103 SET SQL_NOTES='ON' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

#
# Source for table "form_pbl_pices"
#

DROP TABLE IF EXISTS `form_pbl_pices`;
CREATE TABLE `form_pbl_pices` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `picsrc` varchar(50) NOT NULL DEFAULT '0',
  `picname` varchar(50) NOT NULL DEFAULT '0',
  `picheigh` int(4) NOT NULL DEFAULT '0',
  `picwidth` int(4) NOT NULL DEFAULT '0',
  `picmark` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

#
# Data for table "form_pbl_pices"
#

INSERT INTO `form_pbl_pices` VALUES (1,'pices(0)','pices0',127,190,'图片0'),(2,'pices(1)','pices1',285,190,'图片1'),(3,'pices(2)','pices2',128,190,'图片2'),(4,'pices(3)','pices3',285,190,'图片3'),(5,'pices(4)','pices4',242,190,'图片4'),(6,'pices(5)','pices5',227,190,'图片5'),(7,'pices(6)','pices6',158,190,'图片6'),(8,'pices(7)','pices7',235,190,'图片7'),(9,'pices(8)','pices8',127,190,'图片8'),(10,'pices(9)','pices9',127,190,'图片9'),(11,'pices(10)','pices10',128,190,'图片10'),(12,'pices(11)','pices11',285,190,'图片11'),(13,'pices(12)','pices12',128,190,'图片12'),(14,'pices(13)','pices13',241,190,'图片13'),(15,'pices(14)','pices14',163,190,'图片14'),(16,'pices(15)','pices15',285,190,'图片15'),(17,'pices(16)','pices16',142,190,'图片16'),(18,'pices(17)','pices17',143,190,'图片17'),(19,'pices(18)','pices18',126,190,'图片18'),(20,'pices(19)','pices19',187,190,'图片19'),(21,'pices(20)','pices20',120,190,'图片20'),(22,'pices(21)','pices21',256,190,'图片21'),(23,'pices(22)','pices22',190,190,'图片22'),(24,'pices(23)','pices23',126,190,'图片23'),(25,'pices(24)','pices24',190,190,'图片24'),(26,'pices(25)','pices25',285,190,'图片25'),(27,'pices(26)','pices26',134,190,'图片26'),(28,'pices(27)','pices27',155,190,'图片27'),(29,'pices(28)','pices28',126,190,'图片28'),(30,'pices(29)','pices29',143,190,'图片29'),(31,'pices(30)','pices30',288,190,'图片30'),(32,'pices(31)','pices31',127,190,'图片31'),(33,'pices(32)','pices32',276,190,'图片32'),(34,'pices(33)','pices33',155,190,'图片33'),(35,'pices(34)','pices34',127,190,'图片34'),(36,'pices(35)','pices35',190,190,'图片35'),(37,'pices(36)','pices36',190,190,'图片36'),(38,'pices(37)','pices37',190,190,'图片37'),(39,'pices(38)','pices38',240,190,'图片38'),(40,'pices(39)','pices39',190,190,'图片39'),(41,'pices(40)','pices40',190,190,'图片40'),(42,'pices(41)','pices41',289,190,'图片41'),(43,'pices(42)','pices42',206,190,'图片42'),(44,'pices(43)','pices43',285,190,'图片43'),(45,'pices(44)','pices44',272,190,'图片44'),(46,'pices(45)','pices45',285,190,'图片45'),(47,'pices(46)','pices46',121,190,'图片46'),(48,'pices(47)','pices47',185,190,'图片47'),(49,'pices(48)','pices48',190,190,'图片48'),(50,'pices(49)','pices49',126,190,'图片49');

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
