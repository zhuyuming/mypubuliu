<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="../js/ajax.js"></script>
<title>瀑布流</title>
<link rel="stylesheet" type="text/css" href="../css/pbl.css"/>
<!--[if IE 7]>
	<!-- 适合于IE7 -->
	<link rel="stylesheet" type="text/css" href="../css/pblie.css" />
<![endif]--> 　
<!--[if IE 8]>
	<!-- 适合于IE8 -->
	<link rel="stylesheet" type="text/css" href="../css/pblie.css" />
<![endif]--> 　
<!--[if lte IE 6]>
 　　<!-- 适合于IE6及以下 --> 　　
 　　   <link rel="stylesheet" type="text/css" href="../css/pblie.css" /> 　　
<![endif]-->
</head>

<body>
	<div id="prinwall">
	<ul id="imglist">
	</ul>
	<div id="loadingdiv" class="loader">
		<img alt="正在加载中" src="../imges/ajax-loader.gif">
		正在加载中....
	</div>
</div>
</body>
</html>